package com.metadata.adv.manager.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DateTools.Resolution;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Searcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jeecms.cms.Constants;
import com.jeecms.cms.entity.main.Content;
import com.jeecms.cms.lucene.LuceneContent;
import com.jeecms.cms.manager.main.ContentMng;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.web.springmvc.RealPathResolver;
import com.metadata.adv.manager.CmsAdvSearchMng;

public class CmsAdvSearchMngImpl implements CmsAdvSearchMng{
	
	@Autowired
	ContentMng contentMng;

	@Autowired
	private RealPathResolver realPathResolver;
	
	
	/**
	 * 用于获取检索结果数据
	 */
	@Override
	public Pagination getSearchPage(String[] sfields, String[] keywords, String[] selrelatin, Integer siteId, Integer channelId,String startDate, String endDate, int pageNo, int pageSize) {
		/*try {
			// 获取luncene 索引文件位置
			String path = realPathResolver.get(Constants.LUCENE_PATH);
			Directory dir = new SimpleFSDirectory(new File(path));
			Searcher searcher = new IndexSearcher(dir);
			// 创建查询分析器材
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
			// 创建查询条件 2015-10-30
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date _startDate = null;
			if (StringUtils.isNotBlank(startDate)) {
				_startDate = sdf.parse(startDate);
			}
			Date _endDate = null;
			if (StringUtils.isNotBlank(endDate)) {
				_endDate = sdf.parse(endDate);
			}
			Query query = this.createQuery(sfields, keywords,selrelatin,siteId, channelId,  _startDate, _endDate, analyzer);
			// 获取查询结果文档
			TopDocs docs = searcher.search(query, pageNo * pageSize);
			ScoreDoc[] hits = docs.scoreDocs;
			int endIndex = pageNo * pageSize;
			int len = hits.length;
			if (endIndex > len) {
				endIndex = len;
			}
			// 获取查询结果文档，并且取出分页段所的content的id
			List<Integer> list = new ArrayList<Integer>(pageSize);
			for (int i = (pageNo - 1) * pageSize; i < endIndex; i++) {
				Document d = searcher.doc(hits[i].doc);
				list.add(Integer.valueOf(d.getField("id").stringValue()));
			}
			// 封装分页对象，填充content对象list，返回结果
			Pagination page = new Pagination(pageNo, pageSize, docs.totalHits,
					list);
			List<?> ids = page.getList();
			List<Content> contents = new ArrayList<Content>(ids.size());
			for (Object id : ids) {
				contents.add(contentMng.findById((Integer) id));
			}
			page.setList(contents);
			return page;
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return null;
	}

	/**
	 * 用于统计高级检索数量
	 */
	@Override
	public int getSearchCount(String[] sfields, String[] keywords, String[] selrelatin, Integer siteId, Integer channelId,String startDate, String endDate) {
		try {
			// 获取luncene 索引文件位置
			String path = realPathResolver.get(Constants.LUCENE_PATH);
			Directory dir = new SimpleFSDirectory(new File(path));
			Searcher searcher = new IndexSearcher(dir);
			// 创建查询分析器材
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
			// 创建查询条件 2015-10-30
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date _startDate = null;
			if (StringUtils.isNotBlank(startDate)) {
				_startDate = sdf.parse(startDate);
			}
			Date _endDate = null;
			if (StringUtils.isNotBlank(endDate)) {
				_endDate = sdf.parse(endDate);
			}
			Query query = this.createQuery(sfields, keywords,selrelatin,siteId, channelId, _startDate, _endDate, analyzer);
			//获取查询结果文档
			TopDocs docs = searcher.search(query, 1);
			return docs.totalHits;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 创建查询对象
	 * @param sfields
	 * @param keywords
	 * @param _startDate
	 * @param _endDate
	 * @param analyzer
	 * @return Query
	 * @throws ParseException 
	 */
	private Query createQuery(String[] sfields, String[] keywords,String[] selrelatins, Integer siteId, Integer channelId, Date startDate, Date endDate, Analyzer analyzer) throws ParseException {
		BooleanQuery bq = new BooleanQuery();
		Query q;
		// 循环查询条件并组合
		if(sfields!=null&&keywords!=null)//&&selrelatins!=null
		for (int i = 0; i < keywords.length; i++) {
			if(StringUtils.isNotBlank(keywords[i])){
				if("keyword".equals(sfields[i])){
					//如果是关键词查询
					q = MultiFieldQueryParser.parse(Version.LUCENE_30, keywords[i],
							LuceneContent.QUERY_FIELD, LuceneContent.QUERY_FLAGS, analyzer);
				}else{
					q = MultiFieldQueryParser.parse(Version.LUCENE_30, keywords[i],
							new String[]{sfields[i]}, new BooleanClause.Occur[]{BooleanClause.Occur.SHOULD}, analyzer);
					// new MultiFieldQueryParser(Version.LUCENE_30,keywords[i],analyzer).p;
				}
				
				/*if ("and".equals(selrelatins[i])) {
					bq.add(q, BooleanClause.Occur.MUST);
				} else if ("or".equals(selrelatins[i])) {
					bq.add(q, BooleanClause.Occur.SHOULD);
				} else if ("not".equals(selrelatins[i])) {
					bq.add(q, BooleanClause.Occur.MUST_NOT);
				}*/
			}
		}
		if (siteId != null) {// site
			q = new TermQuery(new Term("siteId", siteId.toString()));
			bq.add(q, BooleanClause.Occur.MUST);
		}
		if (channelId != null) {// channel(一篇文章可以多个channel)
			q = new TermQuery(new Term("channelIdArray", channelId.toString()));
			bq.add(q, BooleanClause.Occur.MUST);
		}
		// 发布时间的范围查询
		if (startDate != null || endDate != null) {
			String start = null;
			String end = null;
			if (startDate != null) {
				start = DateTools.dateToString(startDate, Resolution.DAY);
			}
			if (endDate != null) {
				end = DateTools.dateToString(endDate, Resolution.DAY);
			}
			q = new TermRangeQuery("releaseDate", start, end, true, true);
			if ("and".equals(selrelatins[selrelatins.length-1])) {
				bq.add(q, BooleanClause.Occur.MUST);
			} else if ("or".equals(selrelatins[selrelatins.length-1])) {
				bq.add(q, BooleanClause.Occur.SHOULD);
			} else if ("not".equals(selrelatins[selrelatins.length-1])) {
				bq.add(q, BooleanClause.Occur.MUST_NOT);
			}
		}
		return bq;
	}

}
