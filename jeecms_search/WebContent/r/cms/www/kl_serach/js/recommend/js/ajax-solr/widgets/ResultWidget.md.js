(function ($) {

AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
  start: 0,


  afterRequest: function () {
    $(this.target).empty();
    for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
      var doc = this.manager.response.response.docs[i];
      $(this.target).append(this.template(doc));

      /*var items = [];
       items = items.concat(this.facetLinks('topics', doc.topics));
       items = items.concat(this.facetLinks('organisations', doc.organisations));
       items = items.concat(this.facetLinks('exchanges', doc.exchanges));*/

      /*var $links = $('#links_' + doc.id);
      $links.empty();
      for (var j = 0, m = items.length; j < m; j++) {
        $links.append($('<li></li>').append(items[j]));
      }*/
    }
  },

  template: function (doc) {

    var output = '<li><a href="javascript:void(0);">'+doc.Title+'</a> </li>';
   
    return output;
  },

  init: function () {
    /*$(document).on('click', 'a.more', function () {
      var $this = $(this),
          span = $this.parent().find('span');

      if (span.is(':visible')) {
        span.hide();
        $this.text('more');
      }
      else {
        span.show();
        $this.text('less');
      }

      return false;
    });*/
  }
});
})(jQuery);