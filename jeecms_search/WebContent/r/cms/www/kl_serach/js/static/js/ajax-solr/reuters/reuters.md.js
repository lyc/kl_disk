var Manager;
//var solrUrl="http://127.0.0.1:8074/solr/";


(function ($) {

    $(function () {
        Manager = new AjaxSolr.Manager({
            solrUrl: solr_url
        });
        Manager.init();
        Manager.store.addByValue('q', '*:*');
        Manager.store.addByValue('rows', '10');
        Manager.doRequest();

        Manager.addWidget(new AjaxSolr.ResultWidget({
            id: 'result',
            target: '#docs'
        }));
        
        Manager.addWidget(new AjaxSolr.PagerWidget({
      	  id: 'pager',
      	  target: '#pager',
      	  prevLabel: '上一页',
      	  nextLabel: '下一页',
      	  innerWindow: 1,
      	  renderHeader: function (perPage, offset, total,QTime) {
      	    //$('#pager-header').html($('<span></span>').text('displaying ' + Math.min(total, offset + 1) + ' to ' + Math.min(total, offset + perPage) + ' of ' + total));
      		//time
      		$('#total').html(total);
      		$('#time').html(parseInt(QTime)/1000.0);
      	  }
      	}));
    });

   
})(jQuery);
