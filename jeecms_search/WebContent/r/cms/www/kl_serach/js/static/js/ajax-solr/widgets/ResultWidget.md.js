(function ($) {

AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
  start: 0,


  afterRequest: function () {
    $(this.target).empty();
    for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
      var doc = this.manager.response.response.docs[i];
      $(this.target).append(this.template(doc));

      /*var items = [];
       items = items.concat(this.facetLinks('topics', doc.topics));
       items = items.concat(this.facetLinks('organisations', doc.organisations));
       items = items.concat(this.facetLinks('exchanges', doc.exchanges));*/

      /*var $links = $('#links_' + doc.id);
      $links.empty();
      for (var j = 0, m = items.length; j < m; j++) {
        $links.append($('<li></li>').append(items[j]));
      }*/
    }
  },

  template: function (doc) {

    var output = '<li><a class="font-weight font16"' +"title=" + doc.Title.substr(0,50)  +" href="+addr+"detail/index.jhtml?id="+doc.id+"&qt="+qt+'>'+doc.Title + '</a>';
    output += '<a class="download" href="'+doc.Url+'">下载</a>';
    output += '<p>作者：' + doc.authors + '</p>';
    output += '<p>机构：' + doc.publishing + '</p>';
    output += '<p>发布时间:' + doc.publishtime + '</p>';
    if(!(typeof(doc.db)=='undefined'))
    output += '<p>数据库:' + "cnki" + '</p>';
    output += '<p>摘要：' + doc.Description+ '</p></li>';
    return output;
  },

  init: function () {
    /*$(document).on('click', 'a.more', function () {
      var $this = $(this),
          span = $this.parent().find('span');

      if (span.is(':visible')) {
        span.hide();
        $this.text('more');
      }
      else {
        span.show();
        $this.text('less');
      }

      return false;
    });*/
  }
});
})(jQuery);