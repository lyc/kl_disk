(function ($) {

AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
  start: 0,


  afterRequest: function () {
    $(this.target).empty();
    for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
      var doc = this.manager.response.response.docs[i];
      $(this.target).append(this.template(doc));

      /*var items = [];
       items = items.concat(this.facetLinks('topics', doc.topics));
       items = items.concat(this.facetLinks('organisations', doc.organisations));
       items = items.concat(this.facetLinks('exchanges', doc.exchanges));*/

      /*var $links = $('#links_' + doc.id);
      $links.empty();
      for (var j = 0, m = items.length; j < m; j++) {
        $links.append($('<li></li>').append(items[j]));
      }*/
    }
  },

  template: function (doc) {

    var output = '<div class="searchDetail-resourceTitle">';
    output += '<p class="font16 font-weight">《'+doc.Title+'》</p>';
    output += '<span class="font14 fr">浏览次数：100</span>'
    output += '</div>';
    output += '<div class="searchDetail font14">';
    output += '<p><strong class="font-weight">【文献类型】</strong>'+'期刊'+'</p>';
    output += '<p><strong class="font-weight">【作者】</strong>'+doc.authors+'</p>';
    output += '<p><strong class="font-weight">【机构】</strong>'+doc.publishing+'</p>';
    output += '<p><strong class="font-weight">【来源】</strong>'+doc.publocal+'</p>';
    output += '<p><strong class="font-weight">【发行时间】</strong>'+doc.publishtime+'</p>';
    output += '<p><strong class="font-weight">【关键词】</strong>'+doc.Keywords+'</p>';
    output += '<p><strong class="font-weight">【分类号】</strong>'+doc.number+'</p>';
    output += '<p><strong class="font-weight">【摘要】</strong>'+doc.Description+'</p>';
    output += '</div>';
    return output;
  },

  init: function () {
    /*$(document).on('click', 'a.more', function () {
      var $this = $(this),
          span = $this.parent().find('span');

      if (span.is(':visible')) {
        span.hide();
        $this.text('more');
      }
      else {
        span.show();
        $this.text('less');
      }

      return false;
    });*/
  }
});
})(jQuery);