package com.metadata.adv.manager.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Test;
import org.springframework.stereotype.Service;

import com.jeecms.common.page.Pagination;
import com.metadata.adv.manager.CmsAdvSearchMng;
@Service
public class CmsAdvSearchMngImpl implements CmsAdvSearchMng{
	
	
	//以后从配置文件读
	//private static final String urlString = "http://localhost:8074/solr";
	
	/**
	 * 用于获取检索结果数据
	 */
	@Override
	public Pagination getSearchPage(String[] sfields, String[] keywords, int pageNo, int pageSize) {
		try {
			String urlString = "http://localhost:8074/solr";
			SolrServer solr = new HttpSolrServer(urlString);
			SolrQuery solrParams=new SolrQuery();
			
			solrParams.setQuery("id:1");  
			QueryResponse queryResponse=solr.query(solrParams);
			
			SolrDocumentList documentList=queryResponse.getResults();
			// 获取查询结果文档，并且取出分页段所的content的id
			List<Integer> list = new ArrayList<Integer>(pageSize);
			
			// 封装分页对象，填充content对象list，返回结果
			Pagination page = new Pagination(pageNo, pageSize,50,
					list);
			List<?> ids = page.getList();
			
			
		
			return page;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Test
	public void testFind() throws SolrServerException{
		String urlString = "http://localhost:8074/solr";
		SolrServer solr = new HttpSolrServer(urlString);
		
		//以后参数都是通过这个对象去构造...
		SolrQuery solrParams=new SolrQuery();
		
		solrParams.setQuery("description:小键");  
		
		//分页
//		solrParams.setStart(0);
//		
//		solrParams.setRows(10);
		
		//开启高亮...
		solrParams.setHighlight(true);
		
		//高亮显示的格式...
		solrParams.setHighlightSimplePre("<font color='red'>");
		solrParams.setHighlightSimplePost("</font>");
		
		
		
		//我需要那几个字段进行高亮...
		
		solrParams.setParam("hl.fl", "description");
		QueryResponse queryResponse=solr.query(solrParams);
		
		//返回所有的结果...
		SolrDocumentList documentList=queryResponse.getResults();
		
		Map<String, Map<String, List<String>>> maplist=queryResponse.getHighlighting();
		
		//返回高亮之后的结果..
		
		for(SolrDocument solrDocument:documentList){
			Object id=solrDocument.get("id");
//			Object name=solrDocument.get("name");
//			Object content=solrDocument.get("description");
//			System.out.println(id);
//			System.out.println(name);
//			System.out.println(content);
			Map<String, List<String>>  fieldMap=maplist.get(id);
			List<String> stringlist=fieldMap.get("description");
			
			System.out.println(stringlist);
			
		}
		
	}

	/**
	 * 用于统计高级检索数量
	 */
	@Override
	public int getSearchCount(String[] sfields, String[] keywords) {
		
			
		return 0;
	}
	
	public static void main(String[] args) {
		CmsAdvSearchMngImpl cms = new CmsAdvSearchMngImpl();
		String[] files = null;
		String[] keywords = null;
		 int pageNo = 1;
		int pageSize =10;
		cms.getSearchPage(files, keywords, pageNo, pageSize);
	}
}
