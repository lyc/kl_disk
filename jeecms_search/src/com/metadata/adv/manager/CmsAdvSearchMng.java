package com.metadata.adv.manager;

import com.jeecms.common.page.Pagination;

/**
 * 高级检索service
 * @author dqw
 *
 */
public interface CmsAdvSearchMng {
	/**
	 * 用于获取检索结果数据
	 * @param sfield
	 * @param keyword
	 * @param startDate
	 * @param endDate
	 * @param pageNo
	 * @param pageSize
	 * @return Pagination
	 * @author dqw
	 */
	public Pagination getSearchPage(String[] sfield, String[] keyword,int pageNo, int pageSize);
	
	/**
	 * 用于统计高级检索数量
	 * @param sfield
	 * @param keyword
	 * @param starttime
	 * @param endtime
	 * @return int
	 * @author dqw
	 */
	public int getSearchCount(String[] sfield, String[] keyword);
	
	
}
