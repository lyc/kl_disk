package com.metadata.form.action;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeecms.cms.entity.main.Content;
import com.jeecms.cms.manager.main.ContentMng;

/**
 * 根据内容ID获取点击量
 * @param id
 * @return map
 * @author dqw
 *
 */
@Controller
public class FindViewAct {
	/**
	 * 根据文献ID获取一组点击量
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/getViews.jspx")
	@ResponseBody
	public Map<String, Integer> getViewsById(@RequestParam("ids[]") String[] ids) {
		Map<String, Integer> hitCountMap = new HashMap<>();
		if (ids == null || ids.length == 0)  //判断ID是否空
			return null;

		for (String li : ids) {
			Content content = contentMng.findById(Integer.parseInt(String.valueOf(li))); //获取内容ID
			hitCountMap.put(li, content.getViews());  //根据内容ID获取对应的点击量放进Map里
		}
		return hitCountMap;
	}
	
	
	/**
	 * 根据视频ID获取点击量
	 * @param id
	 * @return int
	 * @author dqw
	 */
	@RequestMapping(value = "/getViews2.jspx")
	@ResponseBody
	public Integer getViewsByVedioId(@RequestParam("id") int id){
		//获取ID
		Content content = contentMng.findById(id);
		if(content == null ){
			return  null;
		}
		//获取点击量
		Integer hits = content.getViews();
		return hits;
	}
	
	/**
	 * 根据图片ID获取点击量
	 * @param id
	 * @return int
	 * @author dqw
	 */
	@RequestMapping(value = "/getViews3.jspx")
	@ResponseBody
	public Integer getViewsByPicId(@RequestParam("id") int id){
		//获取ID
		Content content = contentMng.findById(id);
		if(content == null ){
			return  null;
		}
		//获取点击量
		Integer hits = content.getViews();
		return hits;
	}

	@Autowired
	private ContentMng contentMng;
}
