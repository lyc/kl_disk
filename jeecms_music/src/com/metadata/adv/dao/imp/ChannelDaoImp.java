package com.metadata.adv.dao.imp;

import java.util.ArrayList;
import java.util.List;

import com.jeecms.common.hibernate3.HibernateSimpleDao;
import com.metadata.adv.dao.ChannelDao;

public class ChannelDaoImp extends HibernateSimpleDao implements ChannelDao {

	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		String sql = "select count(*) from (select jc.* from jc_channel jc,jc_channel_ext jct where jc.channel_id = jct.channel_id and parent_id in(select channel_id ci FROM jc_channel ))ac where ac.channel_id not in( select jc.parent_id  from jc_channel jc,jc_channel_ext jct where jc.channel_id = jct.channel_id and parent_id in(select channel_id ci FROM jc_channel ))";
		Integer count = null;
		try{
		  count = (Integer) getSession().createSQLQuery(sql).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public List getList() {
		String sql = "select ac.* from (select jc.* from jc_channel jc,jc_channel_ext jct where jc.channel_id = jct.channel_id and parent_id in(select channel_id ci FROM jc_channel ))ac where ac.channel_id not in( select jc.parent_id  from jc_channel jc,jc_channel_ext jct where jc.channel_id = jct.channel_id and parent_id in(select channel_id ci FROM jc_channel ))";
		List lists = new ArrayList<>();
		try{
		  lists =  getSession().createSQLQuery(sql).setFirstResult(0).setMaxResults(12).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return lists;
	}

}
