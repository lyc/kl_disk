package com.metadata.adv.dao;

import java.util.List;

public interface ChannelDao {
	/**
	 * @author lyc
	 * date 20160524 16:31
	 * @return
	 */
	public Integer getCount();
	public List getList();
}
