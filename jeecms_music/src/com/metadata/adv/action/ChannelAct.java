package com.metadata.adv.action;

import static com.jeecms.cms.Constants.TPLDIR_SPECIAL;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeecms.common.page.Pagination;
import com.jeecms.common.web.RequestUtils;
import com.jeecms.common.web.ResponseUtils;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.core.web.util.FrontUtils;
import com.jeecms.core.web.util.URLHelper;
import com.metadata.adv.manager.CmsChannelMng;

/**
 * 高级检索功能Action
 * @author lyc
 *
 */
@Controller
public class ChannelAct {
/*	public static final String LOGIN_INPUT = "/WEB-INF/t/jeecore/login.html";
	public static final String LOGIN_SUCCESS = "/WEB-INF/t/jeecore/login_success.html";
	
	public static final String SEARCH_INPUT = "tpl.searchInput";
	public static final String SEARCH_RESULT = "tpl.searchResult";
	public static final String SEARCH_ERROR = "tpl.searchError";
	public static final String SEARCH_JOB = "tpl.searchJob";
	@Autowired
	private CmsChannelMng cmsAdvSearchMng;
	
	
	@RequestMapping(value = "/toAdvSearchs.jspx")
	public String open(HttpServletRequest request,HttpServletResponse response, ModelMap model){
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		return "WEB-INF/metadata/advSearch/search.html";
	}
	
	
	@RequestMapping(value = "/advSearchList*.jspx")
	public String advSearchList(HttpServletRequest request,HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		String[] sfield = null;
		if (request.getParameter("adv") != null) {
			sfield = String.valueOf(request.getParameter("adv")).split(",");// 查询字段
		}
		String[] keyword = null;
		if (request.getParameter("q") != null) {
			keyword = String.valueOf(request.getParameter("q")).split(",");// 关键字
		}
		
		
		String[] selrelatin = null;
		if (request.getParameter("selrelatins") != null) {
			selrelatin = String.valueOf(request.getParameter("selrelatins")).split(",");// 逻辑关系
		}
		
		
		String starttime = request.getParameter("starttime");// 开始时间
		String endtime = request.getParameter("endtime");// 结束时间
		//Integer channelId = null;
		String channelId = null;
		if (request.getParameter("channelId") != null && !"".equals((String)request.getParameter("channelId"))) {
			channelId = String.valueOf(request.getParameter("channelId"));
			model.addAttribute("channelId",channelId);
		}
		
		Integer pageNo = URLHelper.getPageNo(request) == 0 ? 1 : URLHelper.getPageNo(request);
		Integer pageSize = request.getParameter("pageSize") == null ? 10
				: Integer.parseInt(String.valueOf(request
						.getParameter("pageSize")));
		Pagination page;
		if(StringUtils.isNotBlank(channelId)){
			page = cmsAdvSearchMng.getSearchPage(sfield, keyword,selrelatin, site.getId(), Integer.valueOf(channelId.trim()),starttime, endtime, pageNo, pageSize);
		}else{
		    page = cmsAdvSearchMng.getSearchPage(sfield, keyword,selrelatin, site.getId(), null,starttime, endtime, pageNo, pageSize);
		}
		model.addAttribute("page", page);
		//回显参数
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("sfields", request.getParameter("adv"));
		paramMap.put("keywords", request.getParameter("q"));
		
		model.addAttribute("paramMap",paramMap);
		FrontUtils.frontData(request, model, site);
		FrontUtils.frontPageData(request, model);
		
		// 将request中所有参数保存至model中。
		model.putAll(RequestUtils.getQueryParams(request));
		//FrontUtils.frontData(request, model, site);
		//FrontUtils.frontPageData(request, model);
		String q = RequestUtils.getQueryParam(request, "q");
		
		model.addAttribute("q",q);
		model.addAttribute("filed", request.getParameter("adv"));
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_SPECIAL, SEARCH_RESULT);
		//return "WEB-INF/t/cms/www/kl_disk/special/search_result.html";
	}
	
	@RequestMapping(value = "/advSearchCount.jspx")
	@ResponseBody
	public void advSearchCount(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws JSONException, UnsupportedEncodingException {
		CmsSite site = CmsUtils.getSite(request);
		String[] sfield = null;
		if (request.getParameter("sfields") != null) {
			sfield = String.valueOf(request.getParameter("sfields"))
					.split(",");// 查询字段
		}
		String[] keyword = null;
		if (request.getParameter("keywords") != null) {
			String _keyword =String.valueOf(request.getParameter("keywords"));
			_keyword = new String(_keyword.getBytes("ISO-8859-1"),"UTF-8");
			_keyword = java.net.URLDecoder.decode(_keyword , "UTF-8");
			keyword = _keyword.split(",");// 关键字
		}

		String[] selrelatin = null;
		if (request.getParameter("selrelatins") != null) {
			selrelatin = String.valueOf(
					request.getParameter("selrelatins")).split(",");// 逻辑关系
		}
		String starttime = request.getParameter("starttime");// 开始时间
		String endtime = request.getParameter("endtime");// 结束时间
		String channelIds = request.getParameter("cids");// 获取channels
		// 提供根据多个channelId来批量获取查询
		JSONObject object = new JSONObject();
		if (channelIds != null) {
			String[] _channelIds = channelIds.split(",");
			int allCounts = 0;
			for (int i = 0; i < _channelIds.length; i++) {
				if (StringUtils.isNotBlank(_channelIds[i])) {
					Integer channelId = Integer.parseInt(_channelIds[i]);
					int counts = cmsAdvSearchMng.getSearchCount(sfield,
							keyword, selrelatin, site.getId(), channelId,
							starttime, endtime);
					// 存储每个栏目的查询结果
					object.put(String.valueOf(channelId) + "_counts", counts);
					allCounts = allCounts + counts;
				}
			}
			object.put("allCounts", allCounts);// 存储所有栏目的查询结果和
		} else {
			// 没有栏目传入时候的站点内所有
			int counts = cmsAdvSearchMng.getSearchCount(sfield, keyword,
					selrelatin, site.getId(), null, starttime, endtime);
			object.put("allCounts", counts);
		}
		ResponseUtils.renderJson(response, object.toString());
	}
	
	
	@RequestMapping(value = "/toSiteTop.jspx")
	public String toTop(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		return "WEB-INF/metadata/advSearch/top.html";
	}*/
	@Resource
	private CmsChannelMng cmsChannelMng;
	
	@RequestMapping(value = "/toSiteTop.jspx")
	public String toTop(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		//FrontUtils.frontData(request, model, site);
		List list = cmsChannelMng.getList() ;
		model.addAttribute("list", list);
		
		return "WEB-INF/t/cms/www/kl_music/channel/kl_yy.html";
	}
}
