package com.metadata.adv.manager;

import java.util.List;

import com.jeecms.common.page.Pagination;

public interface CmsChannelMng {
	
	/**
	 * @author lyc
	 * date 20160524 16:31
	 * @return
	 */
	public Integer getCount();
	public List getList();
	
}
