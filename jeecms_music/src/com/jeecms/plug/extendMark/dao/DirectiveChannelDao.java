package com.jeecms.plug.extendMark.dao;

import com.jeecms.common.page.Pagination;

public interface DirectiveChannelDao {

    public Pagination getPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, int orderBy,boolean isRecommemd, int pageNo, int pageSize);

}