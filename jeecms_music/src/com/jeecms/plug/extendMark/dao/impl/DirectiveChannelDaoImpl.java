package com.jeecms.plug.extendMark.dao.impl;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.common.page.Pagination;
import com.jeecms.plug.extendMark.dao.DirectiveChannelDao;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DirectiveChannelDaoImpl extends HibernateBaseDao<Channel, Integer>
        implements DirectiveChannelDao {


    @Override
    public Pagination getPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, int orderBy, boolean isRecommemd, int pageNo, int pageSize) {
        String sql = this.getPageForTagSQl(parentId, isleaf, orderBy, isRecommemd);
        Session session = getSession();
        int totalCount = Integer.parseInt(session.createSQLQuery("select count(1) as count from (" + sql + ") count_t").list().get(0).toString());
        Pagination p = new Pagination(pageNo, pageSize, totalCount);
        if (totalCount < 1) {
            p.setList(new ArrayList());
            return p;
        }
        SQLQuery sQLQuery = session.createSQLQuery(sql);
        List list = sQLQuery.list();
        int startNum = totalCount > p.getFirstResult() ? p.getFirstResult() : totalCount;
        int endNum = totalCount > (p.getFirstResult() + p.getPageSize()) ? (p.getFirstResult() + p.getPageSize()) : totalCount;
        p.setList(list.subList(startNum, endNum));
        return p;
    }


    /**
     * 获取查询栏目的sql语句
     * [这一段是原生的mysql，所以如果要支持其他库，需要重载getPageForTagSQl方法]
     *
     * @param parentId
     * @param isleaf
     * @param orderBy
     * @param isRecommemd
     * @return
     */
    private String getPageForTagSQl(Integer parentId, boolean isleaf, int orderBy, boolean isRecommemd) {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" SELECT t1.channel_id ");
        strBuff.append(" FROM jc_channel t1 ");
        strBuff.append(" LEFT JOIN jc_channel_count t2 ON t1.channel_id = t2.channel_id ");
        strBuff.append(" WHERE 1=1 ");
        //如果存在父栏
        if (parentId != null) {
            String cids = getSession().createSQLQuery(" select (select getChildChannelList(" + parentId + ") id) from dual ").list().get(0).toString();
            String cidsParam="''";
            if (StringUtils.isNotBlank(cids)) {
                String _cids[] = cids.split(",");
                for (int i = 0; i < _cids.length; i++){
                    cidsParam += ",'"+_cids[i]+"'";
                }
            }
            strBuff.append(" AND t1.channel_id in ("+cidsParam+")");
        }
        //是否只需要叶子节点
        if (isleaf) {
            strBuff.append(" AND (select count(1) from jc_channel where parent_id = t1.channel_id) =0 ");
        }
        //是否推荐
        if (isRecommemd) {
            strBuff.append(" AND (SELECT attr_value FROM jc_channel_attr  WHERE attr_name='isRecommemd' AND channel_id = t1.channel_id LIMIT 1) = '是' ");
        }
        //排序方式[1总量 2月 3周 4天]
        switch (orderBy) {
            case 1:
                strBuff.append(" ORDER BY t2.views DESC ");
                break;
            case 2:
                strBuff.append(" ORDER BY t2.views_month DESC ");
                break;
            case 3:
                strBuff.append(" ORDER BY t2.views_week DESC ");
                break;
            case 4:
                strBuff.append(" ORDER BY t2.views_day DESC ");
                break;
        }
        return strBuff.toString();
    }


    /**
     * 获得Dao对于的实体类
     *
     * @return
     */
    @Override
    protected Class<Channel> getEntityClass() {
        return Channel.class;
    }

}