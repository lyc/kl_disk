package com.jeecms.plug.extendMark.action.directive;

import static com.jeecms.cms.Constants.TPL_STYLE_LIST;
import static com.jeecms.cms.Constants.TPL_SUFFIX;
import static com.jeecms.common.web.Constants.UTF8;
import static com.jeecms.common.web.freemarker.DirectiveUtils.OUT_PAGINATION;
import static com.jeecms.core.web.util.FrontUtils.PARAM_STYLE_LIST;
import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.jeecms.plug.extendMark.manager.DirectiveChannelMng;
import org.apache.commons.lang.StringUtils;

import com.jeecms.cms.action.directive.abs.AbstractChannelDirective;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.web.freemarker.DirectiveUtils;
import com.jeecms.common.web.freemarker.ParamsRequiredException;
import com.jeecms.common.web.freemarker.DirectiveUtils.InvokeType;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.util.FrontUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 栏目分页标签
 */
public class NewChannelPageDirective extends AbstractChannelDirective {

    @Autowired
    private DirectiveChannelMng directiveChannelMng;

    /**
     * 模板名称
     */
    public static final String TPL_NAME = "channel_page";

    /**
     * 是否叶子节点
     */
    public static final String IS_LEAF = "isleaf";

    /**
     * 按照访问量排序[1总量 2月 3周 4天]
     */
    public static final String ORDER_BY = "orderBy";

    /**
     * 是否推荐
     */
    public static final String IS_RECOMMEND = "isRecommemd";


    @SuppressWarnings("unchecked")
    public void execute(Environment env, Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body) throws TemplateException, IOException {
        CmsSite site = FrontUtils.getSite(env);
        Integer parentId = DirectiveUtils.getInt(PARAM_PARENT_ID, params);
        Integer siteId = DirectiveUtils.getInt(PARAM_SITE_ID, params);
        boolean hasContentOnly = getHasContentOnly(params);
        boolean isleaf = DirectiveUtils.getBool(IS_LEAF, params);
        Integer orderBy = DirectiveUtils.getInt(ORDER_BY, params);
        boolean isRecommemd = DirectiveUtils.getBool(IS_RECOMMEND, params);
        Pagination page;
        page = directiveChannelMng.getPageForTag(parentId, hasContentOnly, isleaf,isRecommemd,orderBy,
                FrontUtils.getPageNo(env), FrontUtils.getCount(params));
        Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(
                params);
        paramWrap.put(OUT_PAGINATION, DEFAULT_WRAPPER.wrap(page));
        Map<String, TemplateModel> origMap = DirectiveUtils
                .addParamsToVariable(env, paramWrap);
        InvokeType type = DirectiveUtils.getInvokeType(params);
        String listStyle = DirectiveUtils.getString(PARAM_STYLE_LIST, params);
        if (InvokeType.sysDefined == type) {
            if (StringUtils.isBlank(listStyle)) {
                throw new ParamsRequiredException(PARAM_STYLE_LIST);
            }
            env.include(TPL_STYLE_LIST + listStyle + TPL_SUFFIX, UTF8, true);
        } else if (InvokeType.userDefined == type) {
            if (StringUtils.isBlank(listStyle)) {
                throw new ParamsRequiredException(PARAM_STYLE_LIST);
            }
            FrontUtils.includeTpl(TPL_STYLE_LIST, site, env);
        } else if (InvokeType.custom == type) {
            FrontUtils.includeTpl(TPL_NAME, site, params, env);
        } else if (InvokeType.body == type) {
            body.render(env.getOut());
        } else {
            throw new RuntimeException("invoke type not handled: " + type);
        }
        DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);
    }
}
