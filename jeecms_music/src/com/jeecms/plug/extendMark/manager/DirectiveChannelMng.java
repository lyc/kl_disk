package com.jeecms.plug.extendMark.manager;

import com.jeecms.common.page.Pagination;

/**
 * 栏目管理接口
 * author dingshuang
 * date 2016-06-06
 */
public interface DirectiveChannelMng {

    public Pagination getPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, boolean isRecommemd, int orderBy, int pageNo, int pageSize);

}