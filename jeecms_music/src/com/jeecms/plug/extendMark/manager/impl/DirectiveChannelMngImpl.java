package com.jeecms.plug.extendMark.manager.impl;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.manager.main.ChannelMng;
import com.jeecms.common.page.Pagination;
import com.jeecms.plug.extendMark.dao.DirectiveChannelDao;
import com.jeecms.plug.extendMark.manager.DirectiveChannelMng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 栏目管理接口
 * author dingshuang
 * date 2016-06-06
 */
@Service
@Transactional
public class DirectiveChannelMngImpl implements DirectiveChannelMng {

    @Autowired
    DirectiveChannelDao directiveChannelDao;

    @Autowired
    ChannelMng channelMng;

    @Override
    public Pagination getPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf,boolean isRecommemd, int orderBy, int pageNo, int pageSize) {
        Pagination p = directiveChannelDao.getPageForTag(parentId, hasContentOnly, isleaf, orderBy,isRecommemd, pageNo, pageSize);
        List cList = this.getChannelListByIds(p.getList(), new ArrayList());
        p.setList(cList);
        return p;
    }

    /**
     * 根据id查询栏目
     *
     * @param list
     * @param cList
     * @return
     */
    private List getChannelListByIds(List list, List cList) {
        if (list != null && list.size() > 0) {
            for (Object obj : list) {
                Channel c =channelMng.findById(Integer.parseInt(String.valueOf(obj)));
                cList.add(c);
            }
        }
        return cList;
    }

}